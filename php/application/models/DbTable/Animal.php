<?php

class Application_Model_DbTable_Animal extends Zend_Db_Table_Abstract
{
    protected $_name = 'animal';
    protected $_primary = 'id';

    public function getAll()
    {
        return $this->fetchAll();
    }
}
