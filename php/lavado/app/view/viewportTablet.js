/*
 * File: app/view/viewportTablet.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.0.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.0.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('siadno.view.viewportTablet', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.viewportTablet',

    requires: [
        'siadno.view.PanelRegistroServicios',
        'siadno.view.PanelComplementoInf',
        'siadno.view.PanelResumen'
    ],

    layout: {
        type: 'fit'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'tabpanel',
                    activeTab: 0,
                    items: [
                        {
                            xtype: 'PanelRegistroServicios'
                        },
                        {
                            xtype: 'PanelComplementoInf'
                        },
                        {
                            xtype: 'PanelResumen'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});