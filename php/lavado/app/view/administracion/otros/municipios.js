/*
 * File: app/view/administracion/otros/municipios.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.0.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.0.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('siadno.view.administracion.otros.municipios', {
    extend: 'Ext.window.Window',
    alias: 'widget.AdministracionOtrosMunicipios',

    LISTAR_DEPARTAMENTOS: 201,
    LISTAR_MUNICIPIOS: 301,
    height: 600,
    width: 800,
    resizable: false,
    layout: {
        align: 'stretch',
        type: 'hbox'
    },
    closable: false,
    iconCls: 'icon-flag_france',
    title: 'Paises, Departamentos y Municipios',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            listeners: {
                beforeshow: {
                    fn: me.onWindowBeforeShow,
                    scope: me
                }
            },
            items: [
                {
                    xtype: 'gridpanel',
                    accionEditar: 102,
                    accionBorrar: 103,
                    flex: 1,
                    draggable: true,
                    title: 'Paises',
                    store: 'dumyStore',
                    columns: [
                        {
                            xtype: 'gridcolumn',
                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                if(Ext.isEmpty(value)){
                                    return "<font style='color:gray'>--Nuevo Pais--</font>";    
                                }

                                return value;
                            },
                            width: 130,
                            dataIndex: 'nombre',
                            text: 'Pais',
                            editor: {
                                xtype: 'textfield'
                            }
                        },
                        {
                            xtype: 'gridcolumn',
                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                if(Ext.isEmpty(value)){
                                    return "<font style='color:gray'>--Nueva Nacionalidad--</font>";    
                                }

                                return value;
                            },
                            dataIndex: 'nacionalidad',
                            text: 'Nacionalidad',
                            editor: {
                                xtype: 'textfield'
                            }
                        }
                    ],
                    viewConfig: {
                        plugins: [
                            Ext.create('Ext.grid.plugin.DragDrop', {
                                enableDrop: false
                            })
                        ]
                    },
                    plugins: [
                        Ext.create('Ext.grid.plugin.RowEditing', {
                            listeners: {
                                edit: {
                                    fn: me.onGridroweditingpluginEdit,
                                    scope: me
                                }
                            }
                        })
                    ],
                    selModel: Ext.create('Ext.selection.RowModel', {
                        listeners: {
                            select: {
                                fn: me.onRowselectionmodelSelect,
                                scope: me
                            }
                        }
                    })
                },
                {
                    xtype: 'gridpanel',
                    accionEditar: 202,
                    accionBorrar: 203,
                    flex: 1,
                    title: 'Departamentos',
                    store: 'dumyStore',
                    columns: [
                        {
                            xtype: 'gridcolumn',
                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                if(Ext.isEmpty(value)){
                                    return "<font style='color:gray'>--Nuevo Departamento--</font>";    
                                }

                                return value;
                            },
                            width: 130,
                            dataIndex: 'nombre',
                            text: 'Departamento',
                            editor: {
                                xtype: 'textfield'
                            }
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'gentilicio',
                            text: 'Gentilicio',
                            editor: {
                                xtype: 'textfield'
                            }
                        }
                    ],
                    viewConfig: {
                        plugins: [
                            Ext.create('Ext.grid.plugin.DragDrop', {
                                enableDrop: false
                            })
                        ]
                    },
                    plugins: [
                        Ext.create('Ext.grid.plugin.RowEditing', {
                            listeners: {
                                edit: {
                                    fn: me.onGridroweditingpluginEdit1,
                                    scope: me
                                }
                            }
                        })
                    ],
                    selModel: Ext.create('Ext.selection.RowModel', {
                        listeners: {
                            select: {
                                fn: me.onRowselectionmodelSelect1,
                                scope: me
                            }
                        }
                    })
                },
                {
                    xtype: 'gridpanel',
                    accionEditar: 302,
                    accionBorrar: 303,
                    flex: 1,
                    title: 'Municipios',
                    store: 'dumyStore',
                    columns: [
                        {
                            xtype: 'gridcolumn',
                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                if(Ext.isEmpty(value)){
                                    return "<font style='color:gray'>--Nuevo Municipio--</font>";    
                                }

                                return value;
                            },
                            width: 130,
                            dataIndex: 'nombre',
                            text: 'Municipio',
                            editor: {
                                xtype: 'textfield'
                            }
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'gentilicio',
                            text: 'Gentilicio',
                            editor: {
                                xtype: 'textfield'
                            }
                        }
                    ],
                    viewConfig: {
                        plugins: [
                            Ext.create('Ext.grid.plugin.DragDrop', {
                                enableDrop: false
                            })
                        ]
                    },
                    plugins: [
                        Ext.create('Ext.grid.plugin.RowEditing', {
                            listeners: {
                                edit: {
                                    fn: me.onGridroweditingpluginEdit2,
                                    scope: me
                                }
                            }
                        })
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    flex: 1,
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'buttongroup',
                            title: 'Acciones',
                            columns: 4,
                            layout: {
                                columns: 2,
                                type: 'table'
                            },
                            items: [
                                {
                                    xtype: 'button',
                                    aacion: 'borrar',
                                    iconCls: 'icon-bin',
                                    text: 'Borrar',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClickBorrar14,
                                            scope: me
                                        },
                                        render: {
                                            fn: me.onButtonRender,
                                            scope: me
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'buttongroup',
                            columns: 3,
                            layout: {
                                columns: 3,
                                type: 'table'
                            },
                            items: [
                                {
                                    xtype: 'button',
                                    iconCls: 'icon-door_out',
                                    text: 'Salir',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClickSalir1,
                                            scope: me
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    },

    onWindowBeforeShow: function(component, eOpts) {
        //this.LISTAR_DEPARTAMENTOS=201;
        //this.LISTAR_MUNICIPIOS=301;

        var clase=Ext.ClassManager.get("siadno.store.administracion.otros.paises");
        if(Ext.isEmpty(clase)){
            Ext.define('siadno.store.administracion.otros.paises', {
                extend: 'Ext.data.Store',

                constructor: function(cfg) {
                    var me = this;
                    cfg = cfg || {};
                    me.callParent([Ext.apply({
                        storeId: Ext.id(),
                        idLocal: -1,
                        proxy: {                    
                            type: 'ajax',
                            url: 'clases/interfaces/mantenimiento/local/otros/InterfazMunicipiosDepartamentosPaises.php',
                            reader: {
                                type: 'json',
                                idProperty: 'idpais',
                                messageProperty: 'msg',
                                root: 'data'
                            }
                        },
                        fields: [
                        {
                            name: 'idpais'
                        },
                        {
                            name: 'nombre'
                        },
                        {
                            name: 'nacionalidad'
                        }
                        ]
                    }, cfg)]);
                }
            });
        }

        clase=Ext.ClassManager.get("siadno.store.administracion.otros.departamentos");
        if(Ext.isEmpty(clase)){
            Ext.define('siadno.store.administracion.otros.departamentos', {
                extend: 'Ext.data.Store',

                constructor: function(cfg) {
                    var me = this;
                    cfg = cfg || {};
                    me.callParent([Ext.apply({
                        storeId: Ext.id(),
                        idLocal: -1,
                        proxy: {                    
                            type: 'ajax',
                            url: 'clases/interfaces/mantenimiento/local/otros/InterfazMunicipiosDepartamentosPaises.php',
                            reader: {
                                type: 'json',
                                idProperty: 'iddepartamento',
                                messageProperty: 'msg',
                                root: 'data'
                            }
                        },
                        fields: [
                        {
                            name: 'iddepartamento'
                        },
                        {
                            name: 'idpais'
                        },
                        {
                            name: 'nombre'
                        },
                        {
                            name: 'gentilicio'
                        }
                        ]
                    }, cfg)]);
                }
            });
        }

        clase=Ext.ClassManager.get("siadno.store.administracion.otros.municipios");
        if(Ext.isEmpty(clase)){
            Ext.define('siadno.store.administracion.otros.municipios', {
                extend: 'Ext.data.Store',

                constructor: function(cfg) {
                    var me = this;
                    cfg = cfg || {};
                    me.callParent([Ext.apply({
                        storeId: Ext.id(),
                        idLocal: -1,
                        proxy: {                    
                            type: 'ajax',
                            url: 'clases/interfaces/mantenimiento/local/otros/InterfazMunicipiosDepartamentosPaises.php',
                            reader: {
                                type: 'json',
                                idProperty: 'idmunicipio',
                                messageProperty: 'msg',
                                root: 'data'
                            }
                        },
                        fields: [
                        {
                            name: 'idmunicipio'
                        },
                        {
                            name: 'iddepartamento'
                        },
                        {
                            name: 'nombre'
                        },
                        {
                            name: 'gentilicio'
                        }
                        ]
                    }, cfg)]);
                }
            });
        }

        var grids=Ext.ComponentQuery.query('AdministracionOtrosMunicipios grid');

        var grid=grids[0];
        grid.reconfigure(Ext.create('siadno.store.administracion.otros.paises'));
        grid.getStore().load({callback:this.nuevoPais});

        grid=grids[1];
        grid.reconfigure(Ext.create('siadno.store.administracion.otros.departamentos'));

        grid=grids[2];
        grid.reconfigure(Ext.create('siadno.store.administracion.otros.municipios'));
    },

    onGridroweditingpluginEdit: function(editor, e, eOpts) {
        var grids=Ext.ComponentQuery.query('AdministracionOtrosMunicipios grid');
        var grid=grids[0];

        var callback=function(params, result){            
            if(!result.success){                                   
                return;
            }

            var selectionModel=grid.getSelectionModel();    
            if(selectionModel.hasSelection( )){    
                var idPais=selectionModel.getSelection()[0].get('idpais');
                this.listarDepartamentos.call(this, idPais);
            }    
            this.nuevoPais();
        }

        siadno.enviarCambiosGrid.call(this, grid, null, 'idpais', callback);
    },

    onRowselectionmodelSelect: function(rowmodel, record, index, eOpts) {
        this.listarDepartamentos(record.get('idpais'));

    },

    onGridroweditingpluginEdit1: function(editor, e, eOpts) {
        var grids=Ext.ComponentQuery.query('AdministracionOtrosMunicipios grid');
        var grid=grids[1];

        var callback=function(params, result){
            if(!result.success){                                   
                return;
            }

            var selectionModel=grid.getSelectionModel();    
            if(selectionModel.hasSelection( )){    
                var idDepartamento=selectionModel.getSelection()[0].get('iddepartamento');
                this.listarMunicipios.call(this, idDepartamento);
            }        
            this.nuevoDepartamento();
        }
        siadno.enviarCambiosGrid.call(this, grid, null, 'iddepartamento', callback);
    },

    onRowselectionmodelSelect1: function(rowmodel, record, index, eOpts) {
        this.listarMunicipios(record.get('iddepartamento'));
    },

    onGridroweditingpluginEdit2: function(editor, e, eOpts) {
        var grid=Ext.ComponentQuery.query('AdministracionOtrosMunicipios grid')[2];         

        var callback=function(params, result){
            if(!result.success){                                   
                return;
            }             
            this.nuevoMunicipio();
        }


        siadno.enviarCambiosGrid.call(this, grid, null, 'idmunicipio', callback);
    },

    onButtonClickBorrar14: function(button, e, eOpts) {
        Ext.MessageBox.alert('Informacion', 'Para borrar, arrastre sobre mi un municipio, departamento o pais');
    },

    onButtonRender: function(component, eOpts) {
        component.dropTarget = Ext.create('Ext.dd.DropTarget', component.el, {ddGroup:'GridDD'});
        component.dropTarget.notifyDrop = function(source, evt, data) {  
            var grids=Ext.ComponentQuery.query('AdministracionOtrosMunicipios grid');

            var registro=data.records[0];
            var idPais=registro.get('idpais');
            var idDepartamento=registro.get('iddepartamento');
            var idMunicipio=registro.get('idmunicipio');

            if(!Ext.isEmpty(idMunicipio) && !Ext.isEmpty(idDepartamento)){        
                siadno.borrarRegistroGrid(grids[2], null, 'idmunicipio');  
            }else{
                if(!Ext.isEmpty(idDepartamento) && !Ext.isEmpty(idPais)){        
                    siadno.borrarRegistroGrid(grids[1], null, 'iddepartamento');  
                }else{            
                    if(!Ext.isEmpty(idPais)){                
                        siadno.borrarRegistroGrid(grids[0], null, 'idpais');  
                    }else{
                        Ext.MessageBox.alert('Error', 'La operacion de borrado no es valida');
                    }
                }
            }    
            return true;
        };
        return true;
    },

    onButtonClickSalir1: function(button, e, eOpts) {
        this.close();
    },

    listarDepartamentos: function(idPais) {
        var grids=Ext.ComponentQuery.query('AdministracionOtrosMunicipios grid');
        var grid=grids[2];

        grid.getStore().removeAll();

        grid=grids[1];
        grid.getStore().removeAll();
        grid.getStore().getProxy().extraParams={accion:this.LISTAR_DEPARTAMENTOS, idpais:idPais};
        grid.getStore().load({callback:this.nuevoDepartamento});
    },

    listarMunicipios: function(idDepartamento) {
        var grid=Ext.ComponentQuery.query('AdministracionOtrosMunicipios grid')[2];
        grid.getStore().removeAll();
        grid.getStore().getProxy().extraParams={accion:this.LISTAR_MUNICIPIOS, iddepartamento:idDepartamento};
        grid.getStore().load({callback:this.nuevoMunicipio});
    },

    nuevoPais: function() {
        var grids=Ext.ComponentQuery.query('AdministracionOtrosMunicipios grid');
        var grid=grids[0];
        siadno.nuevoRegistroGrid(grid, 'idpais');
    },

    nuevoDepartamento: function() {
        var grids=Ext.ComponentQuery.query('AdministracionOtrosMunicipios grid');

        var grid=grids[0];
        var selectionModel=grid.getSelectionModel();

        if(!selectionModel.hasSelection( )){    
            Ext.MessageBox.alert('Error', 'Debe seleccionar un pais al cual agregar departamentos');
            return;
        }

        var idPais=selectionModel.getSelection()[0].get('idpais'); 
        grid=grids[1];
        siadno.nuevoRegistroGrid(grid, 'iddepartamento', {idpais:idPais});
    },

    nuevoMunicipio: function() {
        var grids=Ext.ComponentQuery.query('AdministracionOtrosMunicipios grid');

        var grid=grids[1];
        var selectionModel=grid.getSelectionModel();

        if(!selectionModel.hasSelection()){    
            Ext.MessageBox.alert('Error', 'Debe seleccionar un departamento al cual agregar municipios');
            return;
        }

        var idDepartamento=selectionModel.getSelection()[0].get('iddepartamento'); 
        grid=grids[2];
        siadno.nuevoRegistroGrid(grid, 'idmunicipio', {iddepartamento:idDepartamento});
    }

});