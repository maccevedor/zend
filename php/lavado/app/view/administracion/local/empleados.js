/*
 * File: app/view/administracion/local/empleados.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.0.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.0.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('siadno.view.administracion.local.empleados', {
    extend: 'Ext.window.Window',
    alias: 'widget.AdministracionLocalEmpleados',

    idPerfil: '0',
    cargado: '0',
    AGREGAR_PERFIL: '101',
    QUITAR_PERFIL: '102',
    CAMBIAR_PASSWORD: '103',
    cambiandoIdentificacion: '0',
    height: 480,
    width: 506,
    resizable: false,
    layout: {
        type: 'fit'
    },
    closable: false,
    iconCls: 'icon-group_link',
    title: 'Mis Empleados',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    layout: {
                        type: 'auto'
                    },
                    bodyPadding: 10,
                    url: 'clases/interfaces/mantenimiento/local/InterfazEmpleados.php',
                    items: [
                        {
                            xtype: 'fieldset',
                            padding: '0 10 0 10',
                            title: 'Datos Personales',
                            items: [
                                {
                                    xtype: 'hiddenfield',
                                    anchor: '100%',
                                    fieldLabel: 'Label',
                                    name: 'idpersona'
                                },
                                {
                                    xtype: 'hiddenfield',
                                    anchor: '100%',
                                    fieldLabel: 'Label',
                                    name: 'idusuario'
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    height: 27,
                                    margin: 0,
                                    padding: 0,
                                    layout: {
                                        align: 'stretch',
                                        type: 'hbox'
                                    },
                                    fieldLabel: 'Identificación',
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            margins: '0',
                                            margin: '0 1 0 0',
                                            padding: 0,
                                            width: 68,
                                            labelPad: 0,
                                            name: 'idtipoidentificacion',
                                            value: 0,
                                            editable: false,
                                            displayField: 'abreviatura',
                                            store: 'dumyStore',
                                            valueField: 'idtipoidentificacion',
                                            listeners: {
                                                change: {
                                                    fn: me.onComboboxChange2,
                                                    scope: me
                                                },
                                                select: {
                                                    fn: me.onComboboxSelect1,
                                                    scope: me
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            margins: '0',
                                            margin: '0 1 0 0',
                                            padding: 0,
                                            width: 234,
                                            labelPad: 0,
                                            name: 'identificacion',
                                            enableKeyEvents: true,
                                            hideTrigger: true,
                                            autoSelect: false,
                                            displayField: 'identificacion',
                                            minChars: 5,
                                            queryParam: 'identificacion',
                                            store: 'dumyStore',
                                            valueField: 'identificacion',
                                            listeners: {
                                                keyup: {
                                                    fn: me.onComboboxKeyup,
                                                    scope: me
                                                },
                                                blur: {
                                                    fn: me.onComboboxBlur,
                                                    scope: me
                                                },
                                                select: {
                                                    fn: me.onComboboxSelect,
                                                    scope: me
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'displayfield',
                                            flex: 1,
                                            name: 'digitoverificacion',
                                            value: 'D.V -0'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    fieldLabel: 'Nombres',
                                    name: 'nombres'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    fieldLabel: 'Apellidos',
                                    name: 'apellidos'
                                },
                                {
                                    xtype: 'combobox',
                                    anchor: '100%',
                                    fieldLabel: 'Nacionalidad',
                                    name: 'idpais',
                                    value: 0,
                                    editable: false,
                                    displayField: 'nacionalidad',
                                    forceSelection: true,
                                    store: 'dumyStore',
                                    valueField: 'idpais'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    fieldLabel: 'Dirección',
                                    name: 'direccion'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    fieldLabel: 'Teléfonos',
                                    name: 'telefonos'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    fieldLabel: 'E-Mail',
                                    name: 'email'
                                },
                                {
                                    xtype: 'datefield',
                                    anchor: '100%',
                                    autoShow: true,
                                    fieldLabel: 'Nacimiento',
                                    name: 'fechanacimiento',
                                    format: 'd/m/Y',
                                    submitFormat: 'Y-m-d'
                                },
                                {
                                    xtype: 'radiogroup',
                                    fieldLabel: 'Sexo',
                                    items: [
                                        {
                                            xtype: 'radiofield',
                                            name: 'sexo',
                                            boxLabel: 'Masculino',
                                            checked: true,
                                            inputValue: '1'
                                        },
                                        {
                                            xtype: 'radiofield',
                                            name: 'sexo',
                                            boxLabel: 'Femenino',
                                            inputValue: '2'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'checkboxfield',
                                    anchor: '100%',
                                    fieldLabel: 'Activo?',
                                    name: 'estado',
                                    boxLabel: 'Señale para indicar que esta activo',
                                    inputValue: '1',
                                    uncheckedValue: '0'
                                },
                                {
                                    xtype: 'textareafield',
                                    anchor: '100%',
                                    fieldLabel: 'Observaciones',
                                    name: 'observaciones',
                                    growAppend: ' -'
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'buttongroup',
                            title: 'Acciones',
                            columns: 4,
                            layout: {
                                columns: 4,
                                type: 'table'
                            },
                            items: [
                                {
                                    xtype: 'button',
                                    hidden: true,
                                    iconCls: 'icon-page_add',
                                    text: 'Nuevo',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClickNuevo,
                                            scope: me
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    iconCls: 'icon-page_save',
                                    text: 'Guardar',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClickGuardar,
                                            scope: me
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    hidden: true,
                                    iconCls: 'icon-page_cancel',
                                    text: 'Cancelar',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClickCancelar,
                                            scope: me
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    iconCls: 'icon-page_delete',
                                    text: 'Borrar',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClickBorrar,
                                            scope: me
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'buttongroup',
                            title: 'Especiales',
                            columns: 4,
                            layout: {
                                columns: 4,
                                type: 'table'
                            },
                            items: [
                                {
                                    xtype: 'button',
                                    name: 'cambiarIdentificacion',
                                    enableToggle: true,
                                    iconCls: 'icon-user_edit',
                                    text: 'Cambiar Identificación',
                                    tooltip: 'Habilita el cambio de identificacion para el usuario en la interfaz. Una vez activado puede cambiar la identificacion y luego guardar los datos.',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClickCambiarIdentificacion,
                                            scope: me
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'buttongroup',
                            columns: 2,
                            layout: {
                                columns: 2,
                                type: 'table'
                            },
                            items: [
                                {
                                    xtype: 'button',
                                    iconCls: 'icon-door_out',
                                    text: 'Salir',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClick,
                                            scope: me
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ],
            listeners: {
                beforeshow: {
                    fn: me.onWindowBeforeShow,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    onComboboxChange2: function(field, newValue, oldValue, eOpts) {
        var indice=field.getStore().findExact('idtipoidentificacion', newValue);
        var registro=field.getStore().getAt(indice);
        this.habilitarPersonaNatural(registro.get('espersonanatural'));
        return true;
    },

    onComboboxSelect1: function(combo, records, eOpts) {
        if(this.cambiandoIdentificacion==1){
            return;
        }

        var combo=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=identificacion]')[0];
        this.traerPersona(records[0].get('idtipoidentificacion'), combo.getValue());
    },

    onComboboxKeyup: function(textfield, e, eOpts) {
        if(this.cambiandoIdentificacion==1){
            return;
        }

        siadno.limpiarFormulario('AdministracionLocalEmpleados', null, ['idtipoidentificacion', 'identificacion']);
        var key=e.getKey();
        if(key==13){        
            var comboIdTipoIdentificacion=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=idtipoidentificacion]')[0];
            var comboIdentificacion=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=identificacion]')[0];        
            this.traerPersona(comboIdTipoIdentificacion.getValue(), comboIdentificacion.getValue());
        }
    },

    onComboboxBlur: function(field, eOpts) {
        if(this.cambiandoIdentificacion==1){
            return;
        }
        var hidden=Ext.ComponentQuery.query('AdministracionLocalEmpleados hidden[name=idpersona]')[0];
        var idPersona=hidden.getValue();
        if(Ext.isEmpty(idPersona)){    
            var comboIdTipoIdentificacion=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=idtipoidentificacion]')[0];    
            var comboIdentificacion=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=identificacion]')[0];            
            this.traerPersona(comboIdTipoIdentificacion.getValue(), comboIdentificacion.getValue());    
        }
    },

    onComboboxSelect: function(combo, records, eOpts) {
        combo=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=idtipoidentificacion]')[0];
        combo.setValue(records[0].get('idtipoidentificacion')); 

        var comboIdTipoIdentificacion=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=idtipoidentificacion]')[0];    
        var comboIdentificacion=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=identificacion]')[0];            
        this.traerPersona(comboIdTipoIdentificacion.getValue(), records[0].get('identificacion'));
    },

    onButtonClickNuevo: function(button, e, eOpts) {

    },

    onButtonClickGuardar: function(button, e, eOpts) {
        var callback=function(){    
            var button=Ext.ComponentQuery.query('AdministracionLocalEmpleados button[name=cambiarIdentificacion]')[0];
            button.toggle(false);        
            this.cambiandoIdentificacion=0;    

            var identificacion=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=identificacion]')[0];
            var tipoIdentificacion=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=idtipoidentificacion]')[0];    
            //this.traerPersona(tipoIdentificacion.getValue(),identificacion.getValue());
        };

        siadno.enviarFormulario.call(this, false, 'AdministracionLocalEmpleados form', {accion:siadno.GUARDAR_DATOS}, callback, null);

    },

    onButtonClickCancelar: function(button, e, eOpts) {

    },

    onButtonClickBorrar: function(button, e, eOpts) {
        if(this.cambiandoIdentificacion==1){
            Ext.MessageBox.alert('Informacion', 'Desactive el cambio de identificacion antes de intentar borrar el usuario'); 
            return;
        }
        var callback=function(){        
            var identificacion=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=identificacion]')[0];
            var tipoIdentificacion=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=idtipoidentificacion]')[0];    
            this.traerPersona(tipoIdentificacion.getValue(),identificacion.getValue());
        };

        siadno.enviarFormulario.call(this, false, 'AdministracionLocalEmpleados form', {accion:siadno.BORRAR_DATOS}, callback, null);
    },

    onButtonClickCambiarIdentificacion: function(button, e, eOpts) {
        var hidden=Ext.ComponentQuery.query('AdministracionLocalEmpleados hidden[name=idpersona]')[0];
        var idPersona=hidden.getValue();

        if(Ext.isEmpty(idPersona) || idPersona==0){
            button.toggle(false);
            Ext.MessageBox.alert('Informacion', 'Traiga una persona a la interfaz antes de intentar hacer cambio de identificacion'); 
            return;
        }

        if(this.cambiandoIdentificacion==0){
            this.cambiandoIdentificacion=1;
            button.toggle(true);
            Ext.MessageBox.alert('Informacion', 'Se ha activado el cambio de identificacion. Modifique la identificacion y haga click en guardar.'); 
        }else{
            this.cambiandoIdentificacion=0;
            button.toggle(false);
        }
    },

    onButtonClick: function(button, e, eOpts) {
        this.close();
    },

    onWindowBeforeShow: function(component, eOpts) {
        var clase=Ext.ClassManager.get("siadno.store.sistema.tiposIdentificacion");
        if(Ext.isEmpty(clase)){
            Ext.define('siadno.store.sistema.tiposIdentificacion', {
                extend: 'Ext.data.Store',

                constructor: function(cfg) {
                    var me = this;
                    cfg = cfg || {};
                    me.callParent([Ext.apply({
                        autoLoad: true,
                        storeId: Ext.id(),
                        proxy: {
                            type: 'ajax',
                            url: 'clases/data/sistema/tiposIdentificacion.php',
                            reader: {
                                type: 'json',
                                idProperty: 'idtipoidentificacion',
                                messageProperty: 'msg',
                                root: 'data'
                            }
                        },
                        fields: [
                        {
                            name: 'idtipoidentificacion'
                        },
                        {
                            name: 'nombre'
                        },
                        {
                            name: 'abreviatura'
                        },
                        {
                            name: 'espersonanatural'
                        }
                        ]
                    }, cfg)]);
                }
            });
        }

        clase=Ext.ClassManager.get("siadno.store.sistema.paises");
        if(Ext.isEmpty(clase)){
            Ext.define('siadno.store.sistema.paises', {
                extend: 'Ext.data.Store',

                constructor: function(cfg) {
                    var me = this;
                    cfg = cfg || {};
                    me.callParent([Ext.apply({
                        autoLoad: true,
                        storeId: Ext.id(),
                        proxy: {
                            type: 'ajax',
                            url: 'clases/data/sistema/paises.php',
                            reader: {
                                type: 'json',
                                idProperty: 'idpais',
                                messageProperty: 'msg',
                                root: 'data'
                            }
                        },
                        fields: [
                        {
                            name: 'idpais'
                        },
                        {
                            name: 'nacionalidad'
                        },
                        {
                            name: 'nombre'
                        }
                        ]
                    }, cfg)]);
                }
            });
        }

        clase=Ext.ClassManager.get("siadno.store.sistema.personas");
        if(Ext.isEmpty(clase)){
            Ext.define('siadno.store.sistema.personas', {
                extend: 'Ext.data.Store',

                constructor: function(cfg) {
                    var me = this;
                    cfg = cfg || {};
                    me.callParent([Ext.apply({
                        storeId: Ext.id(),
                        proxy: {
                            type: 'ajax',
                            url: 'clases/data/sistema/personas.php',
                            reader: {
                                type: 'json',
                                idProperty: 'idpersona',
                                messageProperty: 'msg',
                                root: 'data'
                            }
                        },
                        fields: [
                        {
                            name: 'idpersona'
                        },
                        {
                            name: 'idtipoidentificacion'
                        },
                        {
                            name: 'identificacion'
                        },
                        {
                            name: 'abreviatura'
                        },
                        {
                            name: 'nombres'
                        }
                        ]
                    }, cfg)]);
                }
            });
        }

        var combo=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=identificacion]')[0];
        combo.bindStore(Ext.create('siadno.store.sistema.personas'));
        var listConfig={
            loadingText: 'Buscando...',
            emptyText: 'No se encontraron personas.',               
            getInnerTpl: function() {
                return '<div>{abreviatura} {identificacion}</div><div>{nombres}</div>';                    
            }
        };
        combo.listConfig=listConfig;

        combo=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=idtipoidentificacion]')[0];
        combo.bindStore(Ext.create('siadno.store.sistema.tiposIdentificacion'));
        combo.getStore().load(); 

        combo=Ext.ComponentQuery.query('AdministracionLocalEmpleados combobox[name=idpais]')[0];
        combo.bindStore(Ext.create('siadno.store.sistema.paises'));
        combo.getStore().load(); 

    },

    traerPersona: function(idtipoidentificacion, identificacion) {
        siadno.enviarFormulario.call(this, true, 'AdministracionLocalEmpleados form',{idtipoidentificacion:idtipoidentificacion, identificacion:identificacion}, null, null);
    },

    habilitarPersonaNatural: function(habilitar) {
        var campo;

        if(habilitar==1){
            campo=Ext.ComponentQuery.query('AdministracionLocalEmpleados displayfield[name=digitoverificacion]')[0];
            campo.setVisible(false);
            campo=Ext.ComponentQuery.query('AdministracionLocalEmpleados textfield[name=apellidos]')[0];
            campo.setDisabled(false);
            campo=Ext.ComponentQuery.query('AdministracionLocalEmpleados radiofield[name=sexo]');    
            campo[0].setDisabled(false);
            campo[1].setDisabled(false);        
            campo=Ext.ComponentQuery.query('AdministracionLocalEmpleados datefield[name=fechanacimiento]')[0];
            campo.setDisabled(false);    

        }else{
            campo=Ext.ComponentQuery.query('AdministracionLocalEmpleados displayfield[name=digitoverificacion]')[0];
            campo.setVisible(true);
            campo=Ext.ComponentQuery.query('AdministracionLocalEmpleados textfield[name=apellidos]')[0];
            campo.setDisabled(true);
            campo=Ext.ComponentQuery.query('AdministracionLocalEmpleados radiofield[name=sexo]');
            campo[0].setDisabled(true);
            campo[1].setDisabled(true);    
            campo=Ext.ComponentQuery.query('AdministracionLocalEmpleados datefield[name=fechanacimiento]')[0];
            campo.setDisabled(true);
        }

    }

});