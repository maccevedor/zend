/*
 * File: app/view/mantenimiento/local/permisos/perfiles.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.0.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.0.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('siadno.view.mantenimiento.local.permisos.perfiles', {
    extend: 'Ext.window.Window',
    alias: 'widget.MantenimientoLocalPermisosPerfiles',

    idPerfil: '0',
    cargado: '0',
    BORRAR_PERFIL: '2002',
    NUEVO_PERFIL: '2001',
    height: 600,
    width: 640,
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    closable: false,
    iconCls: 'icon-application_osx_cascade',
    title: 'Perfiles',
    maximizable: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'gridpanel',
                    flex: 1,
                    maxHeight: 200,
                    store: 'dumyStore',
                    columns: [
                        {
                            xtype: 'gridcolumn',
                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                if(Ext.isEmpty(value) || value<0){
                                    return "<font style='color:gray'>N.D</font>";    
                                }

                                return value;
                            },
                            width: 83,
                            dataIndex: 'idperfil',
                            text: 'Identificador'
                        },
                        {
                            xtype: 'gridcolumn',
                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                if(Ext.isEmpty(value)){
                                    return "<font style='color:gray'>--Nuevo Perfil--</font>";    
                                }

                                return value;
                            },
                            width: 397,
                            dataIndex: 'nombre',
                            text: 'Nombre del Perfil',
                            editor: {
                                xtype: 'textfield',
                                name: 'nombre'
                            }
                        },
                        {
                            xtype: 'booleancolumn',
                            dataIndex: 'estado',
                            text: 'Activo?',
                            falseText: 'In Activo',
                            trueText: 'Activo',
                            undefinedText: 'No Definido',
                            editor: {
                                xtype: 'checkboxfield',
                                name: 'estado',
                                boxLabel: 'Activo?'
                            }
                        }
                    ],
                    selModel: Ext.create('Ext.selection.RowModel', {

                    }),
                    plugins: [
                        Ext.create('Ext.grid.plugin.RowEditing', {

                        })
                    ],
                    listeners: {
                        select: {
                            fn: me.onGridpanelSelect,
                            scope: me
                        }
                    }
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'treepanel',
                            cargado: 'false',
                            flex: 1,
                            animCollapse: false,
                            iconCls: 'icon-bricks',
                            title: 'Módulos Fuera del Perfil',
                            store: 'mantenimiento.privado.modulosnoenperfil',
                            animate: false,
                            displayField: 'nombre',
                            rootVisible: false,
                            viewConfig: {
                                plugins: [
                                    Ext.create('Ext.tree.plugin.TreeViewDragDrop', {

                                    })
                                ],
                                listeners: {
                                    beforedrop: {
                                        fn: me.onTreedragdroppluginBeforeDrop1,
                                        scope: me
                                    }
                                }
                            },
                            listeners: {
                                itemappend: {
                                    fn: me.onTreepanelItemAppend,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'treepanel',
                            cargado: 'false',
                            flex: 1,
                            animCollapse: false,
                            iconCls: 'icon-bricks',
                            title: 'Módulos Dentro del Perfil',
                            store: 'mantenimiento.privado.modulosenperfil',
                            animate: false,
                            displayField: 'nombre',
                            rootVisible: false,
                            viewConfig: {
                                plugins: [
                                    Ext.create('Ext.tree.plugin.TreeViewDragDrop', {

                                    })
                                ],
                                listeners: {
                                    beforedrop: {
                                        fn: me.onTreedragdroppluginBeforeDrop,
                                        scope: me
                                    }
                                }
                            },
                            listeners: {
                                itemappend: {
                                    fn: me.onTreepanelItemAppend1,
                                    scope: me
                                }
                            }
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    flex: 1,
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'buttongroup',
                            title: 'Perfil',
                            columns: 4,
                            layout: {
                                columns: 4,
                                type: 'table'
                            },
                            items: [
                                {
                                    xtype: 'button',
                                    iconCls: 'icon-page_add',
                                    text: 'Nuevo',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClickNuevo,
                                            scope: me
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    iconCls: 'icon-page_save',
                                    text: 'Guardar',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClickGuardar,
                                            scope: me
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    iconCls: 'icon-page_cancel',
                                    text: 'Cancelar',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClickCancelar,
                                            scope: me
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    iconCls: 'icon-page_delete',
                                    text: 'Borrar',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClickBorrar,
                                            scope: me
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'buttongroup',
                            columns: 2,
                            layout: {
                                columns: 2,
                                type: 'table'
                            },
                            items: [
                                {
                                    xtype: 'button',
                                    iconCls: 'icon-door_out',
                                    text: 'Salir',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClick,
                                            scope: me
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ],
            listeners: {
                show: {
                    fn: me.onWindowShow,
                    scope: me
                },
                destroy: {
                    fn: me.onWindowDestroy,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    onGridpanelSelect: function(rowmodel, record, index, eOpts) {
        this.idPerfil=record.get('idperfil');
        this.verModulosDelPerfil(this.idPerfil);
    },

    onTreedragdroppluginBeforeDrop1: function(node, data, overModel, dropPosition, dropFunction, eOpts) {
        if(overModel.get('idmodulo')<=0){
            return false;
        }
        node=data.records[0];
        var tree=Ext.ComponentQuery.query('MantenimientoLocalPermisosPerfiles treepanel')[0];
        siadno.ajax.call(this, 'clases/interfaces/mantenimiento/local/permisos/InterfazPerfiles.php',{accion:this.BORRAR_PERFIL, idperfil:this.idPerfil, idmodulo:node.get('idmodulo')}, null, {});    
        return true;
    },

    onTreepanelItemAppend: function(nodeinterface, node, index, eOpts) {
        siadno.cambiarIconoNodo(node);
    },

    onTreedragdroppluginBeforeDrop: function(node, data, overModel, dropPosition, dropFunction, eOpts) {
        if(overModel.get('idmodulo')<=0){
            return false;
        }

        node=data.records[0];
        var tree=Ext.ComponentQuery.query('MantenimientoLocalPermisosPerfiles treepanel')[1];
        siadno.ajax.call(this, 'clases/interfaces/mantenimiento/local/permisos/InterfazPerfiles.php',{accion:this.NUEVO_PERFIL, idperfil:this.idPerfil, idmodulo:node.get('idmodulo')}, null, {});    
        return true;
    },

    onTreepanelItemAppend1: function(nodeinterface, node, index, eOpts) {
        siadno.cambiarIconoNodo(node);
    },

    onButtonClickNuevo: function(button, e, eOpts) {
        var grid=Ext.ComponentQuery.query('MantenimientoLocalPermisosPerfiles grid')[0];
        siadno.nuevoRegistroGrid(grid,'idperfil');
    },

    onButtonClickGuardar: function(button, e, eOpts) {
        var grid=Ext.ComponentQuery.query('MantenimientoLocalPermisosPerfiles grid')[0];         
        siadno.enviarCambiosGrid(grid, null, 'idperfil');            
    },

    onButtonClickCancelar: function(button, e, eOpts) {
        var grid=Ext.ComponentQuery.query('MantenimientoLocalPermisosPerfiles grid')[0];
        siadno.rollbackCambiosGrid(grid);         
    },

    onButtonClickBorrar: function(button, e, eOpts) {
        var grid=Ext.ComponentQuery.query('MantenimientoLocalPermisosPerfiles grid')[0];
        siadno.borrarRegistroGrid(grid, null, 'idperfil');    
    },

    onButtonClick: function(button, e, eOpts) {
        this.close();
    },

    onWindowShow: function(component, eOpts) {
        var clase=Ext.ClassManager.get("siadno.store.mantenimiento.local.permisos.perfiles");
        if(Ext.isEmpty(clase)){
            Ext.define('siadno.store.mantenimiento.local.permisos.perfiles', {
                extend: 'Ext.data.Store',

                constructor: function(cfg) {
                    var me = this;
                    cfg = cfg || {};
                    me.callParent([Ext.apply({
                        storeId: Ext.id(),
                        idLocal: -1,
                        proxy: {
                            type: 'ajax',
                            url: 'clases/interfaces/mantenimiento/local/permisos/InterfazPerfiles.php',
                            reader: {
                                type: 'json',
                                idProperty: 'idperfil',
                                messageProperty: 'msg',
                                root: 'data'
                            }
                        },
                        fields: [
                        {
                            name: 'idperfil'
                        },
                        {
                            name: 'nombre'
                        },
                        {
                            name: 'estado',
                            type: 'boolean'
                        }
                        ]
                    }, cfg)]);
                }
            });
        }



        var grid=Ext.ComponentQuery.query('MantenimientoLocalPermisosPerfiles grid')[0];
        grid.reconfigure(Ext.create('siadno.store.mantenimiento.local.permisos.perfiles'));
        grid.getStore().load(); 

        var tree1=Ext.ComponentQuery.query('MantenimientoLocalPermisosPerfiles treepanel')[0];
        tree1.cargado=false;
        var nodoRaiz1=tree1.getRootNode();
        nodoRaiz1.removeAll();

        var tree2=Ext.ComponentQuery.query('MantenimientoLocalPermisosPerfiles treepanel')[1];
        tree2.cargado=false;
        var nodoRaiz2=tree2.getRootNode();                
        nodoRaiz2.removeAll();
    },

    onWindowDestroy: function(component, eOpts) {
        siadno.crearMenu();
    },

    verModulosDelPerfil: function(idPerfil) {
        var grid=Ext.ComponentQuery.query('MantenimientoLocalPermisosPerfiles gridpanel')[0];
        grid.el.mask('Por favor espere', 'x-mask-loading');

        var tree1=Ext.ComponentQuery.query('MantenimientoLocalPermisosPerfiles treepanel')[0];
        tree1.cargado=false;
        tree1.el.mask('Por favor espere', 'x-mask-loading');
        var nodoRaiz1=tree1.getRootNode();
        nodoRaiz1.removeAll();

        var tree2=Ext.ComponentQuery.query('MantenimientoLocalPermisosPerfiles treepanel')[1];
        tree2.cargado=false;
        tree2.el.mask('Por favor espere', 'x-mask-loading');
        var nodoRaiz2=tree2.getRootNode();                
        nodoRaiz2.removeAll();                


        tree1.getStore().getProxy().extraParams={idPerfil:idPerfil, accion:2004};
        tree1.getStore().load({
            scope: this,
            callback: function(records, operation, success){               
                if(Ext.isEmpty(records) || records.length<=0){
                    tree1.cargado=true;
                    if(tree2.cargado){
                        grid.el.unmask();
                    }
                    tree1.el.unmask();                                                                
                    return;
                }

                var callback=function(){
                    tree1.cargado=true;
                    if(tree2.cargado){
                        grid.el.unmask();
                    }
                    tree1.el.unmask();                                                                
                };
                records[0].expand(false, callback);        

            }
        }); 

        tree2.getStore().getProxy().extraParams={idPerfil:idPerfil, accion:2003};                
        tree2.getStore().load({
            scope: this,
            callback: function(records, operation, success){                                
                if(Ext.isEmpty(records) || records.length<=0){
                    tree2.cargado=true;
                    if(tree1.cargado){
                        grid.el.unmask();
                    }
                    tree2.el.unmask(); 
                }
                var callback=function(){
                    tree2.cargado=true;
                    if(tree1.cargado){
                        grid.el.unmask();
                    }
                    tree2.el.unmask();                
                };  
                records[0].expand(false, callback);        
            }
        });
    }

});